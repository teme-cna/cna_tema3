﻿using ChatGui.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ChatGui.Views
{
    /// <summary>
    /// Interaction logic for ChatView.xaml
    /// </summary>
    public partial class ChatView : Window
    {
        public ChatView(object dataContext)
        {
            InitializeComponent();
            DataContext = dataContext;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            (DataContext as ChatViewModel).Client.Disconnect();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (this.WindowState != WindowState.Minimized)
            {
                this.WindowState = WindowState.Minimized;
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if (this.WindowState != WindowState.Maximized)
            {
                this.WindowState = WindowState.Normal;
            }
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Rectangle_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void TextBlock_TargetUpdated(object sender, EventArgs e)
        {
            TextBlock textBlock = (sender as TextBlock);
            string text = textBlock.Text;
            StringBuilder stringBuilder = new StringBuilder(text);
            textBlock.Text = "";
            textBlock.Inlines.Add(CreateInline(text));
        }
        public Inline CreateInline(string text)
        {
            Span inline = new Span();
            char specialCharacter = ' ';
            if (GetFirstSpecialCharacter(text) != null) {
                specialCharacter = (char)GetFirstSpecialCharacter(text);
            }
            if (specialCharacter != ' ' && text.Contains(specialCharacter))
            {
                int startIndex = text.ToString().IndexOf(specialCharacter);
                int endIndex = text.ToString().Substring(startIndex+1).IndexOf(specialCharacter) + startIndex+1;
                if (endIndex != startIndex)
                {
                    if (CheckSpaces(text, startIndex, endIndex))
                    {
                        inline.Inlines.Add(new Run(text.Substring(0, startIndex)));
                        if (specialCharacter == '*')
                        {
                            inline.Inlines.Add(new Bold(CreateInline(text.Substring(startIndex + 1, endIndex - startIndex - 1))));
                        }
                        else if (specialCharacter == '`')
                        {
                            inline.Inlines.Add(new Italic(CreateInline(text.Substring(startIndex + 1, endIndex - startIndex - 1))));
                        }
                        else if (specialCharacter == '_')
                        {
                            inline.Inlines.Add(new Underline(CreateInline(text.Substring(startIndex + 1, endIndex - startIndex - 1))));
                        }
                        else if (specialCharacter == '~')
                        {
                            Inline crossedInline = CreateInline(text.Substring(startIndex + 1, endIndex - startIndex - 1));
                            crossedInline.TextDecorations = TextDecorations.Strikethrough;
                            inline.Inlines.Add(crossedInline);
                        }
                        if (endIndex != text.Length)
                        {
                            inline.Inlines.Add(new Run(text.Substring(endIndex + 1)));
                        }
                    }
                    else
                    {
                        inline.Inlines.Add(new Run(text.Substring(0, startIndex+1)));
                        inline.Inlines.Add(CreateInline(text.Substring(startIndex+1, endIndex - startIndex - 1)));
                        if (endIndex != text.Length)
                        {
                            inline.Inlines.Add(new Run(text.Substring(endIndex)));
                        }
                    }
                }
                else
                {
                    inline.Inlines.Add(new Run(text.Substring(0, startIndex + 1)));
                    inline.Inlines.Add(CreateInline(text.Substring(startIndex + 1, text.Length - startIndex - 1)));
                }
            }
            else
            {
                inline.Inlines.Add(new Run(text));
            }
            return inline;
        }
        public char? GetFirstSpecialCharacter(string text)
        {
            string characters = "*`_~";
            return text.FirstOrDefault(p => characters.Contains(p));
        }
        public bool CheckSpaces(string text,int startIndex,int endIndex)
        {
            if (startIndex != 0) {
                if (!(text[startIndex - 1] == ' ' && text[startIndex+1] != ' '))
                {
                    return false;
                }
            }
            else
            {
                if (!(text[startIndex + 1] != ' '))
                {
                    return false;
                }
            }
            if (endIndex != text.Length-1)
            {
                if (!(text[endIndex - 1] != ' ' && text[endIndex + 1] == ' '))
                {
                    return false;
                }
            }
            else
            {
                if (!(text[endIndex - 1] != ' '))
                {
                    return false;
                }
            }
            return true;
        }

        private bool autoScroll = true;
        private void ScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            ScrollViewer scrollViewer = sender as ScrollViewer;
            if (e.ExtentHeightChange == 0)
            {
                if (scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
                {
                    autoScroll = true;
                }
                else
                {   
                    autoScroll = false;
                }
            }
            if (autoScroll && e.ExtentHeightChange != 0)
            {
                scrollViewer.ScrollToVerticalOffset(scrollViewer.ExtentHeight);
            }
        }
    }
}
