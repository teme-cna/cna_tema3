using Grpc.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChatServer.Protos;
using Google.Protobuf.WellKnownTypes;
using System.Collections.Concurrent;
using Google.Protobuf.Collections;

namespace ChatServer
{
    public class ChatService : ChatServer.Protos.ChatService.ChatServiceBase
    {
        private UserList _userList;
        private readonly ILogger<ChatService> _logger;
        public ChatService(ILogger<ChatService> logger, UserList userList)
        {
            _logger = logger;
            _userList = userList;
        }

        public override Task<Protos.UserList> GetUserList(Empty request, ServerCallContext context)
        {
            RepeatedField<UserCredentials> users = new RepeatedField<UserCredentials>();
            users.AddRange(_userList.users.Keys.Select(p => new UserCredentials() { Username = p }));

            return Task.FromResult(new Protos.UserList() { User = {users} });
        }

        public async override Task ConnectRequest(UserCredentials request, IServerStreamWriter<Message> responseStream, ServerCallContext context)
        {
            string username = request.Username;
            try
            {
                _userList.users.TryAdd(username, responseStream);
                BroadcastMessage(new Message()
                {
                    MessageType = MessageType.Server,
                    Message_ = "has connected.",
                    User = username
                });
                _logger.Log(LogLevel.Information, $"User {username} connected.");
                while(!context.CancellationToken.IsCancellationRequested)
                {
                    await Task.Delay(100);
                }
            }
            catch
            {
                _logger.Log(LogLevel.Error, $"User {username} already existing.");
            }
            
            return;
        }

        public override Task<Empty> ClientSendMessage(Message request, ServerCallContext context)
        {
            if(_userList.users.ContainsKey(request.User))
            {
                BroadcastMessage(request);
                _logger.Log(LogLevel.Information, $"{request.User}:{request.Message_}");
            } else
            {
                _logger.Log(LogLevel.Error, $"User {request.User} not found.");
            }
            
            return Task.FromResult(new Empty());
        }

        public override Task<Empty> DisconnectRequest(UserCredentials request, ServerCallContext context)
        {
            string username = request.Username;
            try
            {
                _userList.users.TryRemove(username, out IServerStreamWriter<Message> retrieved);
                BroadcastMessage(new Message()
                {
                    MessageType = MessageType.Server,
                    Message_ = "has left the chat.",
                    User = username
                });
                _logger.Log(LogLevel.Information, $"User {username} disconnected.");
            }   
            catch
            {
                _logger.Log(LogLevel.Error, $"User {username} not found.");
            }
            return Task.FromResult(new Empty());
        }

        public void BroadcastMessage(Message request)
        {
            foreach (var stream in _userList.users.Values)
            {
                stream.WriteAsync(new Message { User = request.User, MessageType = request.MessageType, Message_ = request.Message_});
            }
        }

    }
}
