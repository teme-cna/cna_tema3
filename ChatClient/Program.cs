﻿using ChatClient.Protos;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ChatClient
{
    class Program
    {
        string username;
        Client client;
        public Program()
        {
            username = Console.ReadLine();
            client = new Client(username);
            client.OnNewMessage = PrintMessageList;
            client.Connect();
        }
        static async Task Main(string[] args)
        {
            Program program = new Program();
            string line;
            while ((line = Console.ReadLine()) != "Disconnect")
            {
                program.client.SendMessage(line);
            }
            program.client.Disconnect();
        }
        public void PrintMessageList()
        {
            Message lastMessage = client.Messages[client.Messages.Count - 1];
            if (lastMessage.MessageType == MessageType.Client)
            {
                Console.WriteLine($"{ lastMessage.User}:{lastMessage.Message_}");
            }
            else
            {
                Console.WriteLine($"{lastMessage.Message_}");
            }
        }
    }
}
