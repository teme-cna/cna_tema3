﻿using ChatClient;
using ChatGui.Helpers;
using ChatGui.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace ChatGui.ViewModels
{
    public class UserCredentialViewModel : INotifyPropertyChanged
    {
        public UserCredentialViewModel()
        {
            ConnectToChatCommand = new RelayCommand(ConnectToChat);
        }
        private string username;
        public string Username
        {
            get
            {
                return username;
            }
            set
            {
                username = value;
                NotifyPropertyChanged();
            }
        }
        public ICommand ConnectToChatCommand { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        public void ConnectToChat()
        {
            if(Username != null) 
            {
                Client client = new Client();
                try
                {
                    if (!client.GetUsers().Contains(Username))
                    {
                        ChatViewModel chatViewModel = new ChatViewModel(Username);
                        ChatView chatView = new ChatView(chatViewModel);
                        chatView.Show();
                        App.Current.Windows[0].Close();
                    }
                }
                catch
                {
                    MessageBox.Show("Server not online!");
                }
            }

        }
        public void NotifyPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
