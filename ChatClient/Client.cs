﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ChatClient.Protos;
using Grpc.Core;
using Grpc.Net.Client;

namespace ChatClient
{
    public class Client
    {
        private ChatService.ChatServiceClient client;
        private GrpcChannel channel;
        private string username;
        private List<Message> messages;
        public IAsyncStreamReader<Message> StreamReader { get; set; }
        public delegate void FuncNewMessage();
        public FuncNewMessage OnNewMessage { get; set; }
        public List<Message> Messages
        {
            get
            {
                return messages;
            }
        }
        public Client()
        {
            channel = GrpcChannel.ForAddress("https://localhost:5001");
            client = new ChatService.ChatServiceClient(channel);
        }
        public Client(string username)
        {
            channel = GrpcChannel.ForAddress("https://localhost:5001");
            client = new ChatService.ChatServiceClient(channel);
            this.username = username;
            messages = new List<Message>();
        }
        public void Connect()
        {
            StreamReader = client.ConnectRequest(new UserCredentials() { Username = username }).ResponseStream;
            _ = Task.Run(async () =>
            {
                while (await StreamReader.MoveNext(cancellationToken: CancellationToken.None))
                {
                    messages.Add(StreamReader.Current);
                    if (OnNewMessage != null)
                    {
                        OnNewMessage();
                    }
                }
            });
        }
        public void SendMessage(string message)
        {
            client.ClientSendMessageAsync(new Message()
            {
                User = username,
                Message_ = message,
                MessageType = MessageType.Client
            });
        }
        public List<string> GetUsers()
        {
            List<string> userCredentials = new List<string>();
            userCredentials = client.GetUserList(new Google.Protobuf.WellKnownTypes.Empty()).User.Select(p=>p.Username).ToList();
            return userCredentials;
        }
        public void Disconnect()
        {
            CancellationToken cancellationToken = new CancellationTokenSource(0).Token;
            StreamReader.MoveNext(cancellationToken);
            client.DisconnectRequestAsync(new UserCredentials() { Username = username });
        }
    }
}
