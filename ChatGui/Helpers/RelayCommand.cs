﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ChatGui.Helpers
{
    class RelayCommand : ICommand
    {
        Action toExecute;

        public RelayCommand(Action toExecute)
        {
            this.toExecute = toExecute;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            toExecute();
        }
    }

    class RelayCommand<T> : ICommand
    {
        Action<T> toExecute;

        public RelayCommand(Action<T> toExecute)
        {
            this.toExecute = toExecute;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            toExecute((T)parameter);
        }
    }
}
