﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ChatClient;
using ChatClient.Protos;
using ChatGui.Helpers;

namespace ChatGui.ViewModels
{
    public class ChatViewModel : INotifyPropertyChanged
    {
        public Client Client { get; set; }
        public ChatViewModel()
        {

        }
        public ICommand SendMessageCommand { get; set; }
        public ChatViewModel(string username)
        {
            SendMessageCommand = new RelayCommand(SendMessage);
            Username = username;
            Client = new Client(username);
            Client.OnNewMessage = RefreshListsOnNewMessage;
            Client.Connect();
        }
        public string Username { get; set; }
        private string message;
        public event PropertyChangedEventHandler PropertyChanged;
        public List<Message> MessageList
        {
            get
            {
                if (Client != null)
                {
                    return Client.Messages.ToList();
                }
                return null;
            }
        }
        private bool isTextBoxFocused;
        public bool IsTextBoxFocused
        {
            get
            {
                return isTextBoxFocused;
            }
            set
            {
                isTextBoxFocused = value;
                NotifyPropertyChanged();
            }
        }
        public List<string> UserList
        {
            get
            {
                if (Client != null)
                {
                    return Client.GetUsers();
                }
                return null;
            }
        }
        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
                NotifyPropertyChanged();
            }
        }
        public void SendMessage()
        {
            if (Message != null)
            {
                IsTextBoxFocused = false;
                Client.SendMessage(Message);
                Message = null;
                IsTextBoxFocused = true;
            }
        }
        public void RefreshListsOnNewMessage()
        {           
            NotifyPropertyChanged("MessageList");
            NotifyPropertyChanged("UserList");
        }
        public void NotifyPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
