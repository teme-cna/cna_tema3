﻿using ChatClient.Protos;
using ChatGui.ViewModels;
using System.Windows;
using System.Windows.Controls;

namespace ChatGui.Helpers
{
    public class MessageListDataTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate
            SelectTemplate(object item, DependencyObject container)
        {
            FrameworkElement element = container as FrameworkElement;
            var dataContext = App.Current.Windows[0].DataContext;
            if ((dataContext as ChatViewModel) == null)
            {
                return element.FindResource("ServerMessage") as DataTemplate;
            }
            var user = (dataContext as ChatViewModel).Username;

            if (element != null && item != null && item is Message)
            {
                Message message = item as Message;
                if (message.MessageType == MessageType.Server)
                {
                    return element.FindResource("ServerMessage") as DataTemplate;
                }

                else if (message.User.Equals(user))
                {
                    return element.FindResource("MyMessage") as DataTemplate;
                }
                else
                {
                    return element.FindResource("AnotherUserMessage") as DataTemplate;
                }

            }

            return null;
        }
    }
}